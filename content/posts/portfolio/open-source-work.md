---
title: "Open Source Contributions"
date: 2020-11-17T14:39:21-05:00
draft: false
ShowTOC: true
---

I'm personally a firm believer in the principles of libre software / open-source
software. Software development is a community effort which should engage both
developers and end-users in a collaborative effort. I've made a few
contributions to software projects over my career. Here are a few of my
favorites.

## Linux Notification Center

[Linux Notification Center](https://github.com/phuhl/linux_notification_center)
is a GTK notification daemon written in haskell for users who like a desktop
with style. It provides an experience similar to more mainstream operating
systems (the notification center in Windows, the notification shade in iOS, etc)
without compromising extensibility, or being tied to other desktop environments.

I was already using it in my personal setup, since linux unfortunately doesn't
have many notification daemons that ship with a notification center, but I
decided to contribute as an excuse to learn haskell over the summer of 2019!

#### My Contributions

- [#72](https://github.com/phuhl/linux_notification_center/pull/72): Added
  support for html entities
- [#65](https://github.com/phuhl/linux_notification_center/pull/65): Added "Do
  not disturb" mode (pause / unpause popups)
- [#74](https://github.com/phuhl/linux_notification_center/pull/74): Added
  configuration of mouse button behavior
- [#78](https://github.com/phuhl/linux_notification_center/pull/78): Improved
  default formatting
- [#79](https://github.com/phuhl/linux_notification_center/pull/79): Fixed a bug
  with icon sizes
- [#87](https://github.com/phuhl/linux_notification_center/pull/87): Wrote
  documentation about advanced features
- [#86](https://github.com/phuhl/linux_notification_center/pull/86): Authored a
  PKGBUILD to deliver updates to arch linux users automatically

## Taffybar

[Taffybar](https://github.com/taffybar/taffybar) is a haskell library that helps
people build custom system bars (think windows taskbar) for linux GTK
environments. By providing a *library* instead of a binary and a config file,
taffybar is able to achieve true customizability outside the bounds of what the
original authors have intended.

I've developed [my own system
bar](https://gitlab.com/myriacore-dotfiles/taffybar) that I use in my dotfiles,
and have since contributed some of my widgets over to taffybar's main
repository.

#### My Contributions

- [#495](https://github.com/taffybar/taffybar/pull/495): Added support for named
  icons.
- [#498](https://github.com/taffybar/taffybar/pull/498): Added a
  [wttr.in](https://github.com/chubin/wttr.in) widget for displaying the
  weather. Previously, the only weather widget available polled raw NOAA data,
  which meant international users were out of luck.

## Ulauncher Emoji Extension

[Ulauncher Emoji](https://github.com/Ulauncher/ulauncher-emoji) is an emoji
extension written for the [Ulauncher search bar](https://ulauncher.io/). It
allows the user to easily search and copy emojis to their system clipboard.

I wanted a ulauncher extension to copy/paste emojis, so this software was
perfect for my dotfiles. However, I'm super used to discord's emoji shortcodes,
and to discord's emoji font. I personally have twemoji installed on my laptop -
I just prefer the style better. So, these served as my main contributions to the
project!

#### My Contributions

- [#8](https://github.com/Ulauncher/ulauncher-emoji/pull/8): Provided twemoji,
  noto emoji, and blobmoji preview fonts.
- [#9](https://github.com/Ulauncher/ulauncher-emoji/pull/9): Added shortcode
  search (e.g. `zap` for :zap: and `tada` for :tada:)
