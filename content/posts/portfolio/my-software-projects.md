---
title: "My Software Projects"
date: 2020-12-04T15:22:57-05:00
draft: false
ShowTOC: true
---

I've worked on a number of software projects independently from school and
employment. Whenever I write *new* software (as opposed to contributing to
*existing* software), it's usually because I couldn't find anything that covered
my personal needs. I've personally found [this style of development](dogfooding)
incredibly rewarding, because it gives me space to practice writing software on
my terms, and it keeps me motivated to make the software better (since I'll be
the one who feels it if it's not that good).

[dogfooding]: https://www.forbes.com/sites/adrianbridgwater/2019/12/09/why-software-organizations-eat-their-own-dog-food/?sh=361ef3a63070

Here are a few of my favorite projects!

### [Gitlab Markdown CI/CD Setup](https://gitlab.com/myriacore/gitlab-markdown-ci)

I write most of my notes for university in gitlab-flavored markdown. However,
due to issues with gitlab such as
[gitlab#243533](https://gitlab.com/gitlab-org/gitlab/-/issues/243533),
[gitlab#215084](https://gitlab.com/gitlab-org/gitlab/-/issues/215084), etc, it
became harder and harder to view my notes in gitlab reliably.

As a workaround, I've decided to host a gitlab pages instance with my notes
rendered as static html. To make it *extra* convenient, I've configured gitlab's
CI/CD to re-generate the site each time I commit. Eventually, I added on enough
convenience features to warrant giving this setup a repository of its own.

This setup is powered by [pandoc](https://pandoc.org/), a tool for converting
between document formats, and the same tool that's used by
[rmarkdown](https://rmarkdown.rstudio.com/) for knitting to HTML, PDF, etc.

I've also [documented a way to reproduce the setup locally on your
machine](https://gist.github.com/MyriaCore/75729707404cba1c0de89cc03b7a6adf), so
I can turn my notes into html without using CI/CD.

I've even played around with making a [live-editor/live-previewer
tool](https://gitlab.com/myriacore/gitlab-markdown-ci/-/issues/2) so users could
view what their markdown content would look like when rendered, although that's
definitely still a work in progress.

### [Texpander](https://gitlab.com/myriacore/texpander)

Texpander started off as a fork of [Lee Blue's
texpander](https://github.com/leehblue/texpander/), but I've added onto it so
much that it's basically unrecognizable from its original 

Texpander is a text expander for Linux, meant to allow
[TextExpander](https://textexpander.com/) snippets to work on Linux, since
the original TextExpander was only ever written for MacOS. It served both as a
fun way for me to learn bash programming in the summer of 2019, and an honestly
useful program that I was looking for anyways. 

The original only supported literal text replacement with
[zenity](https://help.gnome.org/users/zenity/stable/), but I've since changed
the snippet-choosing UI to use [dmenu](https://linux.die.net/man/1/dmenu) so the
snippet-choosing process could be snappier, refactored the script into a set of
easily composable bash functions so the script would be easier to work on,
created a snippet generator that can generate an entire directory of snippet
files so it would be easier to edit snippets en-masse, and much more.

Along with other features, such as [variable
substitution](https://github.com/leehblue/texpander/pull/29) (which still hasn't
been merged into Lee Blue's repository at the time of writing) and clipboard and
script expansion (which Lee Blue's version doesn't support), I've found this
project *very* useful for things like fast note-taking. 

## Abandonware

Sometimes the original impetus for developing software fades, or is resolved by
other solutions. I do want to list these projects, since I still value what I
learned from them, but I'm no longer actively developing them. 

### [DuckScheduler](https://gitlab.com/myriacore/duckscheduler)

DuckScheduler was an attempt to really stress-test my polyglot-programming
abilities. It's a schedule generator written in Prolog and Clojure, intended
to generate schedules based on students' preferences, and the classes they need
to take. 

It was intended to use [Logic
Programming](https://en.wikipedia.org/wiki/Logic_programming) to
nondeterministically find class sections that meet students constraints (for
example, the student would be able to specify that they didn't want to take any
sections that start earlier than 9am). 

The system would then use Clojure as the glue - The clojure code would download
semester section data, query the prolog code, then serve the website up to the
user. After finishing the Prolog algorithm, I was going to make a full clojure
web server. 

I ended up designing a correct algorithm, however I couldn't figure out how to
tweak it to make it efficient. I had some ideas, like using constraint logic
programming to limit the search space, but I never got around to it. Plus, as
you rise in seniority at Stevens Institute of Technology, you're allowed to sign
up for courses earlier and earlier, reducing the personal need for such a
service.
