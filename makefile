.PHONY: deploy site

server:
	hugo server -D

site:
	hugo -d /var/www/myriacore/

deploy: # git pull --recurse-submodules
	ssh root@myriaco.re 'cd ~/sites/myriaco.re && git pull && sudo make site'
