# myriaco.re

This is a repository containing my personal website, https://myriaco.re. At the
moment, it's just a static html page displaying socials, relevant links, etc.
However, in the future, I might have it include a blog, shorturl service, and
potentially some other services that I'd find useful. 
